

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		HttpSession session = request.getSession();
		User logSes = (User)session.getAttribute("userInfo");

		if (logSes == null) {
			response.sendRedirect("LoginServlet");
		}

//		HttpSession session = request.getSession(false);
//        if(session == null)
//        {
//            response.sendRedirect("LoginServlet");
//            return;
//        }
//        String userName = (String)session.getAttribute("userName");
//        String pass = (String)session.getAttribute("pass");




		// ユーザ一覧情報を取得
		UserDao userDao = new UserDao();
		List<User> userList = userDao.findAll();

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("userList", userList);

		// ユーザ一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
		dispatcher.forward(request, response);


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		//form情報取得
		String loginId = request.getParameter("login-id");
		String name = request.getParameter("user-name");
		String dateSt = request.getParameter("date-start");
		String dateEd = request.getParameter("date-end");

//		//検索処理
//		UserDao userDao = new UserDao();
//		if(!loginId.equals(null)) {
//			List<User> a = userDao.findAll();
//			for(User user : a) {
//				user.getId()
//			}
//		}

		UserDao userDao = new UserDao();
		List<User> userList = userDao.findByName(loginId , name , dateSt , dateEd);


		request.setAttribute("userList", userList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
		dispatcher.forward(request, response);

	}

}
