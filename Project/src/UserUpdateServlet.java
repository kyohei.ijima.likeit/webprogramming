
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		User logSes = (User)session.getAttribute("userInfo");

		if (logSes == null) {
			response.sendRedirect("LoginServlet");
		}


		request.setCharacterEncoding("UTF-8");

		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");
		// TODO  未実装：idを引数にして、idに紐づくユーザ情報を出力する
		UserDao userDao = new UserDao();
		User user = userDao.userDetail(id);

		// ユーザ情報をリクエストスコープにセットしてjspにフォワード
		request.setAttribute("userDetail", user);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		//入力値取得
		int id = Integer.parseInt(request.getParameter("id"));
		String password = request.getParameter("password");
		String password1 = request.getParameter("password1");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");

		//パスワードとパスワード（確認）の入力が異なる場合
		if (!password.equals(password1)) {
			request.setAttribute("errMsgPass", "パスワードとパスワード（確認）の入力が異なります。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//未入力項目がある場合
		boolean isCheckOK = true;
		String[] signUpForm = { name, birthDate };
		for (int i = 0; i < signUpForm.length; i++) {
			if (signUpForm[i].equals("")) {
				isCheckOK = false;
			}
		}
		if (!isCheckOK) {
			request.setAttribute("errMsgNull", "未入力項目があります。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//更新
		UserDao userDao = new UserDao();
		if (password.equals("")) {
			userDao.UpdateUser(id, name, birthDate);
			response.sendRedirect("UserListServlet");
			return;
		}else if(!password.equals("")) {
			userDao.UpdateUser(id, password, name, birthDate);
			response.sendRedirect("UserListServlet");
			return;
		}

	}
}
