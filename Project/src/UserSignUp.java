
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserSignUp
 */
@WebServlet("/UserSignUp")
public class UserSignUp extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserSignUp() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {


		HttpSession session = request.getSession();
		User logSes = (User)session.getAttribute("userInfo");

		if (logSes == null) {
			response.sendRedirect("LoginServlet");
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userSignUp.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		//既に登録されているログインIDが入力された場合
		String loginId = request.getParameter("loginId");

		UserDao userDao = new UserDao();
		boolean a = userDao.findSameLoginId(loginId);

		if (a == false) {
			request.setAttribute("errMsgLog", "すでに登録されているログインIDがあります。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userSignUp.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//パスワードとパスワード（確認）の入力が異なる場合
		String password = request.getParameter("password");
		String password1 = request.getParameter("password1");

		if (!password.equals(password1)) {
			request.setAttribute("errMsgPass", "パスワードとパスワード（確認）の入力が異なります。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userSignUp.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//未入力項目がある場合
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");

		boolean isCheckOK = true;
		String[] signUpForm = { loginId, password, password1, name, birthDate };
		for (int i = 0; i < signUpForm.length; i++) {
			if (signUpForm[i].equals("")) {
				isCheckOK = false;

			}
		}

		if(!isCheckOK) {
			request.setAttribute("errMsgNull", "未入力項目があります。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userSignUp.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//新規登録
		//		String loginId = request.getParameter("loginId");
		//		String password = request.getParameter("password");
		//		String name = request.getParameter("name");
		//		String birthDate = request.getParameter("birthDate");

		//		UserDao userDao = new UserDao();
		userDao.InsertUser(loginId, password, name, birthDate);

		response.sendRedirect("UserListServlet");

	}

}
