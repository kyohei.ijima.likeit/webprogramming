<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ一覧</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/userList.css" rel="stylesheet" type="text/css" />

</head>
<body>

	<ul class="navbar navbar-dark bg-dark justify-content-end">
		<li class="nav-item"><a class="navbar-brand" href="#">${userInfo.name}
				さん</a></li>
		<li class="nav-item"><a href="LogoutServlet"
			class="nav-link navbar-brand">ログアウト</a></li>
	</ul>
	<h1>ユーザ一覧</h1>

	<div class="col-9">
		<a href="UserSignUp" class='userSignUp'>新規登録</a>
	</div>

	<form action='UserListServlet' method='post'>
		<div class="form-group row col-5 mx-auto mt-3">
			<label for="inputPassword" class="col-sm-2 col-form-label">ログインID</label>
			<div class="col-sm-10">
				<input type="text" name="login-id" class="form-control">
			</div>
		</div>

		<div class="form-group row row col-5 mx-auto mt-3">
			<label for="inputPassword" class="col-sm-2 col-form-label">ユーザ名</label>
			<div class="col-sm-10">
				<input type="text" name="user-name" class="form-control">
				</div>
		</div>

		<div class="form-group row row col-5 mx-auto mt-3">
			<label for="inputPassword" class="col-sm-2 col-form-label">生年月日</label>
			<div class="生年月日 row">
				<div class="col-sm-5 ml-3">
					<input type="date" name="date-start" class="form-control">
				</div>
				~
				<div class="col-sm-5">
					<input type="date" name="date-end" class="form-control">
				</div>
			</div>
		</div>

		<div class="col-6 mx-auto">
			<input type="submit" value='検索' class="btn-clipboard float-right"
				title="" data-original-title="Copy to clipboard mt-50">
		</div>
	</form>

	<div class="col-8 mx-auto">
		<p class="box"></p>
	</div>


	<table border class="col-6 mx-auto mt-5">
		<tr>
			<th>ログインID</th>
			<th>ユーザ名</th>
			<th>生年月日</th>
			<th></th>
		</tr>

		<c:forEach var="user" items="${userList}">
			<tr>
				<td>${user.loginId}</td>
				<td>${user.name}</td>
				<td>${user.birthDate}</td>


			<c:choose>
			<c:when test="${userInfo.loginId == 'admin'}">
	   		 <td>
					<a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
                    <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
                    <a class="btn btn-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>
				</td>
	   		</c:when>
			<c:otherwise>
				<td>
					<a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>

					<c:if test="${userInfo.loginId == user.loginId}">
						<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
					</c:if>
				</td>
			</c:otherwise>
			</c:choose>

			</tr>
		</c:forEach>



	</table>


</body>
</html>