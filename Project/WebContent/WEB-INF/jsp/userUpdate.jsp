<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ情報詳細参照</title>
    <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <style>
    h1{
        font-size: 50px;
        position:relative;
        margin-top: 50px;
        text-align:center;
    }

    </style>
</head>
	<body>

        <ul class="navbar navbar-dark bg-dark justify-content-end">
		<li class="nav-item"><a class="navbar-brand" href="#">${userInfo.name}
				さん</a></li>
		<li class="nav-item"><a href="LogoutServlet"
			class="nav-link navbar-brand">ログアウト</a></li>
	</ul>

	<c:if test="${errMsgPass != null}" >
	  	  <div class="alert alert-danger" role="alert">
			  ${errMsgPass}
			</div>
			</c:if>

	<c:if test="${errMsgNull != null}" >
	  	  <div class="alert alert-danger" role="alert">
			  ${errMsgNull}
			</div>
		   </c:if>

    <h1>ユーザ情報更新</h1>

<form action="UserUpdateServlet" method="post">
  <div class="form-group row col-5 mx-auto mt-5">
    <label for="inputPassword" class="col-sm-2 col-form-label">ログインID</label>
    <div class="col-sm-10">
      <label for="inputPassword" class="col-sm-8 float-right col-form-label">${userDetail.loginId}</label>
    </div>
  </div>
  <div class="form-group row row col-5 mx-auto mt-5">
    <label for="inputPassword" class="col-sm-2 col-form-label">パスワード</label>
    <div class="col-sm-10">
      <input type="password" class="form-control col-8 float-right" name="password">
    </div>
  </div>
  <div class="form-group row col-5 mx-auto mt-5">
    <label for="inputPassword" class="col-sm-2 col-form-label">パスワード（確認）</label>
  <div class="col-sm-10">
      <input type="password" class="form-control col-8 float-right" name="password1">
    </div>
  </div>
<div class="form-group row row col-5 mx-auto mt-5">
    <label for="inputPassword" class="col-sm-2 col-form-label">ユーザ名</label>
    <div class="col-sm-10">
      <input type="text" value="${userDetail.name}" class="form-control col-8 float-right" name="name">
    </div>
  </div>
 <div class="form-group row row col-5 mx-auto mt-5">
    <label for="inputPassword" class="col-sm-2 col-form-label">生年月日</label>
    <div class="col-sm-10">
      <input type="text" value="${userDetail.birthDate}" class="form-control col-8 float-right" name="birthDate">
    </div>
  </div>

　<div class="col-1 mx-auto mt-5">
    <input type="submit" value="更新" class="btn-clipboard float-right col-9 mt-3">
    </div>
    <input type="hidden" value="${userDetail.id}" name="id">
    </form>

 <div class="col-5 mx-auto mt-5">
   <a href="UserListServlet" class="btn btn-link float-left mt-5">戻る</a>
    </div>


	</body>
</html>