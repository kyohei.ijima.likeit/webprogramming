<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ログイン画面</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"	  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
     <style type="text/css">
  	h1{
        font-size: 50px;
        position:relative;
        margin-top: 80px;
        text-align:center;
  	}

	</style>

</head>
<body>

	<c:if test="${errMsg != null}" >
	  	  <div class="alert alert-danger" role="alert">
			  ${errMsg}
			</div>
		</c:if>

    <h1>ログイン画面</h1>
<form action="LoginServlet?useSSL=false" method="post">
  <div class="form-group row col-5 mx-auto mt-5">
    <label for="inputPassword" class="col-sm-2 col-form-label">ログインID</label>
    <div class="col-sm-10">
      <input type="loginId" class="form-control" id="inputloginId" name="loginId">
    </div>
  </div>
  <div class="form-group row col-5 mx-auto mt-5">
    <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" id="inputPassword" name="password">
    </div>
  </div>
    <div class="row">
    <div class="col-sm-1 mx-auto mt-5">
        <input type="submit" value="ログイン" class="btn btn-primary">
   </div>
    </div>
    </form>
</body>
</html>